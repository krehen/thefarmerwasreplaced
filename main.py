wantMoney = 100000
wantWood = 20000
wantCarrot = 20000
wantPumpkin = 9000
wantTanks = 200
wantPower = 3000

while True:
	for x in range(get_world_size()):
		for y in range(get_world_size()):
			
			if get_pos_x() == 0 and get_pos_y() == 0:
				do_a_flip()
				if get_ground_type() != Grounds.Soil:
					till()
					do_a_flip()
				if num_items(Items.Sunflower_Seed) == 0:
					trade(Items.Sunflower_Seed, 10)
				if can_harvest():
					harvest()
				else:
					plant(Entities.Sunflower)
				waterThis()		
				
				
				if can_harvest():
					if get_entity_type() == Entities.Sunflower:
						Petals = measure()
						print(Petals)
						harvest()
					elif get_entity_type != Entities.Sunflower:
						harvest()
				else:
					waterThis()
				
				
			if can_harvest():
				harvest()
			
			if num_items(Items.Hay) < wantMoney:
				if can_harvest():
					harvest()
				
			elif num_items(Items.Wood) < wantWood:
				
				if get_pos_x() % 2 == 0:
					if get_pos_y() % 2 == 1:
						plant(Entities.Tree)
						waterThis()
				if get_pos_x() % 2 == 1:
					if get_pos_y() % 2 == 0:
						plant(Entities.Tree)
						waterThis()
				
			elif num_items(Items.Carrot) < wantCarrot:
				if get_ground_type() != Grounds.Soil:
					till()
				plant(Entities.Carrots)
				waterThis()
			
			elif num_items(Items.Pumpkin) < wantPumpkin:
				if get_ground_type() != Grounds.Soil:
					till()
				plant(Entities.Pumpkin)
				waterThis()
	
				
			move(North)
		move(East)
	# Maintenance nach Full-Loop
	einkaufen(Items.Carrot_Seed, 250)
	einkaufen(Items.Pumpkin_Seed, 250)	
	einkaufen(Items.Sunflower_Seed, 250)		
	einkaufen(Items.Empty_Tank, 250)