max_petals = 0
petal_list = []
flower_cords = []
hightes_x = 0
hightes_y = 0

while True:
    for xx in range(get_world_size()):
        for yy in range(get_world_size()):

            x = get_pos_x()
            y = get_pos_y()

            if x == hightes_x and y == hightes_y:
                if can_harvest():
                    harvest()

            if get_ground_type() != Grounds.Soil:
                till()
            if can_harvest():
                Petals = measure()
                if Petals >= max_petals:
                    max_petals = Petals
                    hightes_x = x
                    highes_y = y
                    quick_print(max_petals)
                    
            else:
                if num_items(Items.Sunflower_Seed) == 0:
                    trade(Items.Sunflower_Seed, 20)
                plant(Entities.Sunflower)
                waterThis()
            # do_a_flip()
        move(North)
    move(East)
